// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import 'font-awesome/less/font-awesome.less';
import 'mdi/css/materialdesignicons.min.css';
import 'vue-wysiwyg/dist/vueWysiwyg.css';

import _ from 'lodash';
import wysiwyg from 'vue-wysiwyg';
import tinymce from 'vue-tinymce-editor';
import VueClipboard from 'vue-clipboard2';
import vSelect from 'vue-select';

import FormBuilder from './components/FormBuilder'
import formGroup from './components/FormGroup'

Vue.use(VueClipboard);
Vue.use(wysiwyg);

Vue.component('formGroup', formGroup);

Vue.component('v-select', vSelect);
Vue.component('tinymce', tinymce);

global.jQuery = require('jquery');
var $ = global.jQuery;
window.$ = $;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App, FormBuilder }
})