// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import 'font-awesome/less/font-awesome.less';
import 'mdi/css/materialdesignicons.min.css';
import 'vue-wysiwyg/dist/vueWysiwyg.css';

import _ from 'lodash';
import wysiwyg from 'vue-wysiwyg';
import VueClipboard from 'vue-clipboard2';
import vSelect from 'vue-select';

import FormBuilder from './components/FormBuilder'
import formGroup from './components/FormGroup'
Vue.use(VueClipboard);
Vue.use(wysiwyg);
Vue.component('formGroup', formGroup);

Vue.component('v-select', vSelect);

global.jQuery = require('jquery');
var $ = global.jQuery;
window.$ = $;

export default FormBuilder
export {FormBuilder}