# vue-form-builder

> Drag and drop form builder that saves in JSON

## Example Usage in the Wild

### Basic
```
<form-builder :form_json="form_json" v-on:form_json="updateFormJson" :show_json_inspector="true" > </form-builder>
```
### Limit Form builder to show certain elements
```
//script
element_whitelist = ['header', 'html', 'radio', 'select', 'checkbox'];
//html
<form-builder :form_json="form_json" v-on:form_json="updateFormJson" :show_json_inspector="true" :settings="{element_whitelist: element_whitelist}"> </form-builder>
```

## Component Props
| prop | description |
| ------ | ------ |
| form_json | pre_configured form that can be added to autoload a form that can then be editable (JSON array) |
| show_json_inspector | show the json viewer of the currently configured form, Should be moved to settings eventually|
| settings | settings for the form (JSON Object) see below |


## Component Settings
| setting | description |
| ------ | ------ |
| settings.element_whitelist | array of element names to show on the form builder (way to limit which elements are availble for build |




## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


# Similar Projects

Here are some similar projects that we might look to for inspiration:

* jsonforms.io: https://github.com/eclipsesource/jsonforms (this one supports Vue apparently)
* react-jsonschema-form: https://github.com/rjsf-team/react-jsonschema-form (react-based, started as mozilla project I believe)
* formidable: not exactly a similar project, but the interface is a useful reference 
* More libraries: https://react.libhunt.com/react-jsonschema-form-alternatives
